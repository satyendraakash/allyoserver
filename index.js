const port      =   process.env.PORT || 8080;
const express   =   require('express');
const app       =   express();
const fetch     =   require('node-fetch');
const cheerio   =   require('cheerio');
const url       =   require('url');
var cors        =   require('cors');

app.use(cors());
app.use(express.json());

app.post("/preview", async(req, res)=>{
    const fetchURL = req.body.url;
    
    const resp  =   await fetch(fetchURL);
    const html  =   await resp.text();
    const $     =   cheerio.load(html);

    let title       = $('title').first().text();
    let description = $(`meta[name=description]`).attr('content') || $(`meta[property="og:description"]`).attr('content') || $(`meta[property="twitter:description"]`).attr('content');
    let thumbImage  = $(`meta[name=image]`).attr('content') || $(`meta[property="og:image"]`).attr('content') || $(`meta[property="twitter:image"]`).attr('content');
    let hostName    = url.parse(fetchURL).hostname;   
    let respObj     = {title, description, thumbImage, hostName};
    res.status(200).json(respObj);
});

app.listen(port, ()=>{
    console.log(`Server running at ${port}`);
});
